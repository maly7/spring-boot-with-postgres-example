CREATE TABLE event (
  event_id          SERIAL PRIMARY KEY,
  event_name        VARCHAR(255) NOT NULL,
  event_description VARCHAR(255)
)