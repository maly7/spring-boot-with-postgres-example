package com.github.maly7.exception.event;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EventFileUploadFailureException extends Exception {
    public EventFileUploadFailureException() {
    }

    public EventFileUploadFailureException(String message) {
        super(message);
    }

    public EventFileUploadFailureException(String message, Throwable cause) {
        super(message, cause);
    }
}
