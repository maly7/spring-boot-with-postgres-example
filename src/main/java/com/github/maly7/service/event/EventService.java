package com.github.maly7.service.event;

import com.github.maly7.domain.event.Event;
import com.github.maly7.domain.event.EventRepository;
import com.github.maly7.exception.event.EventFileUploadFailureException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EventService {
    private static final Logger LOG = LoggerFactory.getLogger(EventService.class);

    private EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Event createEvent(Event newEvent) {
        return eventRepository.save(newEvent);
    }

    public List<Event> createEvents(List<Event> events) {
        return events.stream()
                .map(this::createEvent)
                .collect(Collectors.toList());
    }

    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    public Optional<Event> getEvent(int eventId) {
        return Optional.ofNullable(eventRepository.findOne(eventId));
    }

    public List<Event> createEventsFromFile(MultipartFile file) throws EventFileUploadFailureException {
        try {
            CSVParser records = CSVFormat.DEFAULT.withHeader("name", "description").parse(new InputStreamReader(file.getInputStream()));
            return records.getRecords().stream()
                    .map(this::createEventFromCSVRecord)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            LOG.error("Unable to parse file", e);
            throw new EventFileUploadFailureException("File upload failed", e);
        }
    }

    private Event createEventFromCSVRecord(CSVRecord csvRecord) {
        return createEvent(new Event(csvRecord.get("name"), csvRecord.get("description")));
    }
}
