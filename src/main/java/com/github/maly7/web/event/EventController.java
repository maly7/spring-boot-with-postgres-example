package com.github.maly7.web.event;

import com.github.maly7.domain.event.Event;
import com.github.maly7.exception.event.EventFileUploadFailureException;
import com.github.maly7.exception.event.EventNotFoundException;
import com.github.maly7.service.event.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/event")
public class EventController {

    private EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping(produces = {APPLICATION_JSON_UTF8_VALUE, APPLICATION_JSON_VALUE})
    public List<Event> getEvents() {
        return eventService.getAllEvents();
    }

    @GetMapping(path = "{id}", produces = {APPLICATION_JSON_UTF8_VALUE, APPLICATION_JSON_VALUE})
    public Event getEventById(@PathVariable(name = "id") Integer eventId) throws EventNotFoundException {
        Optional<Event> event = eventService.getEvent(eventId);

        if (event.isPresent()) {
            return event.get();
        }

        throw new EventNotFoundException("No Event found for the given id: " + eventId);
    }

    @PostMapping(consumes = {APPLICATION_JSON_UTF8_VALUE, APPLICATION_JSON_VALUE}, produces = {APPLICATION_JSON_VALUE, APPLICATION_JSON_UTF8_VALUE})
    public Event createEvent(@RequestBody Event event) {
        return eventService.createEvent(event);
    }

    @PostMapping(path = "/upload", produces = {APPLICATION_JSON_VALUE, APPLICATION_JSON_UTF8_VALUE})
    public List<Event> createEventsFromFile(@RequestParam("file") MultipartFile file) throws EventFileUploadFailureException {
        if (file.isEmpty()) {
            return Collections.emptyList();
        }

        return eventService.createEventsFromFile(file);
    }
}
