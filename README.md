#Getting Started
1. Install [PostgresSQL](https://www.postgresql.org/download/)
  
  * Make sure to note the username and password used during installation
  * Create the schema 'sample_schema'
  
2. Set the env variables POSTGRES_USER and POSTGRES_PASSWORD to the values used in setup
1. Run ./gradlew bootRun to start the server

## Resources
* [Spring Boot Reference Guide](https://docs.spring.io/spring-boot/docs/current/reference/html/index.html)
* [Spring Data JPA Reference Guide](http://docs.spring.io/spring-data/jpa/docs/1.11.3.RELEASE/reference/html/)
* [Getting Started with Spring Boot Data JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Flyway](https://flywaydb.org/documentation/)
* [Gradle](https://docs.gradle.org/3.5/userguide/userguide.html)
  
